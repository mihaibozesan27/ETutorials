package backend.entities;

import javax.persistence.*;

import java.io.Serializable;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * Created by mihai on 30.11.2015.
 */
@Entity
@Table(name="enrollment")
public class Enrollment implements Serializable{
    private int enrollmentId;
    private int studentId;
    private int courseId;
    private Byte graduated;
    private int grade;

    public Enrollment() {

    }

    public Enrollment(int enrollmentId, int studentId, int courseId, Byte graduated, int grade) {
        this.enrollmentId = enrollmentId;
        this.studentId = studentId;
        this.courseId = courseId;
        this.graduated = graduated;
        this.grade = grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "enrollment_id", unique = true, nullable = false)
    public int getEnrollmentId() {
        return enrollmentId;
    }

    public void setEnrollmentId(int enrollmentId) {
        this.enrollmentId = enrollmentId;
    }

    @Id
    @Column(name = "student_id")
    public int getStudentId() {
        return studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    @Id
    @Column(name = "course_id")
    public int getCourseId() {
        return courseId;
    }

    public void setCourseId(int courseId) {
        this.courseId = courseId;
    }

    @Basic
    @Column(name = "graduated")
    public Byte getGraduated() {
        return graduated;
    }

    public void setGraduated(Byte graduated) {
        this.graduated = graduated;
    }

    @Basic
    @Column(name = "grade")
    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Enrollment that = (Enrollment) o;

        if (enrollmentId != that.enrollmentId) return false;
        if (graduated != null ? !graduated.equals(that.graduated) : that.graduated != null) return false;
        if (grade != 0 ? grade !=that.grade : that.grade != 0) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = enrollmentId;
        result = 31 * result + (graduated != null ? graduated.hashCode() : 0);
        result = 31 * result + (grade != 0 ? grade : 0);
        return result;
    }
}
