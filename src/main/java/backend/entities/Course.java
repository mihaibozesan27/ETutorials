package backend.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * Created by mihai on 30.11.2015.
 */
@Entity
@Table(name = "course")
public class Course implements Serializable{
    private int courseId;
    private String courseName;
    private int numberOfLectures;
    private Date startDate;
    private Date endDate;
    private String promotionType;
    public Course() {

    }

    public Course(int courseId, String courseName, int numberOfLectures, Date startDate,
                  Date endDate, String promotionType) {
        this.courseId = courseId;
        this.courseName  = courseName;
        this.numberOfLectures = numberOfLectures;
        this.startDate = startDate;
        this.endDate = endDate;
        this.promotionType = promotionType;
    }

    public void setNumberOfLectures(Integer numberOfLectures) {
        this.numberOfLectures = numberOfLectures;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "course_id", unique = true, nullable = false, table = "course")
    public int getCourseId() {
        return courseId;
    }

    public void setCourseId(int courseId) {
        this.courseId = courseId;
    }

    @Basic
    @Column(name = "course_name", nullable = false, length = 45, table = "course")
    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    @Basic
    @Column(name = "number_of_lectures", table = "course")
    public int getNumberOfLectures() {
        return numberOfLectures;
    }

    public void setNumberOfLectures(int numberOfLectures) {
        this.numberOfLectures = numberOfLectures;
    }

    @Basic
    @Column(name = "start_date", table = "course")
    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    @Basic
    @Column(name = "end_date", table = "course")
    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @Basic
    @Column(name = "promotion_type", length = 45, table = "course")
    public String getPromotionType() {
        return promotionType;
    }

    public void setPromotionType(String promotionType) {
        this.promotionType = promotionType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Course course = (Course) o;

        if (courseId != course.courseId) return false;
        if (courseName != null ? !courseName.equals(course.courseName) : course.courseName != null) return false;
        if (numberOfLectures != 0 ? numberOfLectures != course.numberOfLectures : course.numberOfLectures != 0)
            return false;
        if (startDate != null ? !startDate.equals(course.startDate) : course.startDate != null) return false;
        if (endDate != null ? !endDate.equals(course.endDate) : course.endDate != null) return false;
        if (promotionType != null ? !promotionType.equals(course.promotionType) : course.promotionType != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = courseId;
        result = 31 * result + (courseName != null ? courseName.hashCode() : 0);
        result = 31 * result + (numberOfLectures != 0 ? numberOfLectures : 0);
        result = 31 * result + (startDate != null ? startDate.hashCode() : 0);
        result = 31 * result + (endDate != null ? endDate.hashCode() : 0);
        result = 31 * result + (promotionType != null ? promotionType.hashCode() : 0);
        return result;
    }
}
