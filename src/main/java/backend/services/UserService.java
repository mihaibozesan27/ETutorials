package backend.services;

import java.util.ArrayList;
import java.util.List;

import backend.dto.UsersDTO;
import backend.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import backend.config.ResourceNotFoundException;
import backend.dto.UserDTO;
import backend.entities.Userr;
import backend.repositories.UserRepository;

@Component
public class UserService {

	@Autowired
	private UserRepository usrRepository;

	public UsersDTO findUserById(int userId) {
		User usr = usrRepository.findById(userId);
		if (usr == null) {
			throw new ResourceNotFoundException();
		}
		UserDTO dto = new UserDTO.Builder()
						.name(usr.getName())
						.city(usr.getCity())
						.address(usr.getAddress())
						.email(usr.getEmail())
						.telephone(usr.getTelephone())
						.create();
		return dto;
	}
	
	
	public List<UserDTO> findAll() {
		List<Userr> users = usrRepository.findAll();
		List<UserDTO> toReturn = new ArrayList<UserDTO>();
		for (Userr user : users) {
			UserDTO dto = new UserDTO.Builder()
						.id(user.getId())
						.name(user.getName())
						.telephone(user.getTelephone())
						.create();
			toReturn.add(dto);
		}
		return toReturn;
	}

}
