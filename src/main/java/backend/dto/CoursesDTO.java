package backend.dto;

import java.sql.Date;

/**
 * Created by mihai on 09.12.2015.
 */
public class CoursesDTO {
    private int courseId;
    private String courseName;
    private int numberOfLectures;
    private Date startDate;
    private Date endDate;
    private String promotionType;

    public CoursesDTO() {

    }
    public CoursesDTO(int courseId, String courseName, int numberOfLectures, Date startDate,
                   Date endDate, String promotionType) {
        this.courseId = courseId;
        this.courseName  = courseName;
        this.numberOfLectures = numberOfLectures;
        this.startDate = startDate;
        this.endDate = endDate;
        this.promotionType = promotionType;
    }

    public int getCourseId() {
        return courseId;
    }

    public void setCourseId(int courseId) {
        this.courseId = courseId;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public int getNumberOfLectures() {
        return numberOfLectures;
    }

    public void setNumberOfLectures(int numberOfLectures) {
        this.numberOfLectures = numberOfLectures;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getPromotionType() {
        return promotionType;
    }

    public void setPromotionType(String promotionType) {
        this.promotionType = promotionType;
    }

    public static class Builder {
        private int nestedCourseId;
        private String nestedCourseName;
        private int nestedNumberOfLectures;
        private Date nestedStartDate;
        private Date nestedEndDate;
        private String nestedPromotionType;

        public Builder courseId(int courseId) {
            this.nestedCourseId = courseId;
            return this;
        }

        public Builder courseName(String courseName) {
            this.nestedCourseName = courseName;
            return this;
        }

        public Builder numberOfLectures(int numberOfLectures) {
            this.nestedNumberOfLectures = numberOfLectures;
            return this;
        }

        public Builder startDate(Date startDate) {
            this.nestedStartDate = startDate;
            return this;
        }

        public Builder endDate(Date endDate) {
            this.nestedEndDate = endDate;
            return this;
        }

        public Builder promotionType(String promotionType) {
            this.nestedPromotionType = promotionType;
            return this;
        }

        public CoursesDTO create() {
            return new CoursesDTO(nestedCourseId, nestedCourseName, nestedNumberOfLectures,
                    nestedStartDate, nestedEndDate, nestedPromotionType);
        }
    }
}
