package backend.dto;

/**
 * Created by mihai on 09.12.2015.
 */
public class EnrollmentsDTO {
    private int enrollmentId;
    private int studentId;
    private int courseId;
    private Byte graduated;
    private int grade;

    public EnrollmentsDTO() {

    }

    public EnrollmentsDTO(int enrollmentId, int studentId, int courseId, Byte graduated, int grade) {
        this.enrollmentId = enrollmentId;
        this.studentId = studentId;
        this.courseId = courseId;
        this.graduated = graduated;
        this.grade = grade;
    }

    public int getEnrollmentId() {
        return enrollmentId;
    }

    public void setEnrollmentId(int enrollmentId) {
        this.enrollmentId = enrollmentId;
    }

    public int getStudentId() {
        return studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    public int getCourseId() {
        return courseId;
    }

    public void setCourseId(int courseId) {
        this.courseId = courseId;
    }

    public Byte getGraduated() {
        return graduated;
    }

    public void setGraduated(Byte graduated) {
        this.graduated = graduated;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    public static class Builder {
        private int nestedEnrollmentId;
        private int nestedStudentId;
        private int nestedCourseId;
        private Byte nestedGraduated;
        private int nestedGrade;

        public Builder enrollmentId(int enrollmentId) {
            this.nestedEnrollmentId = enrollmentId;
            return this;
        }

        public Builder studentId(int studentId) {
            this.nestedStudentId = studentId;
            return this;
        }

        public Builder courseId(int courseId) {
            this.nestedCourseId = courseId;
            return this;
        }

        public Builder graduated(Byte graduated) {
            this.nestedGraduated = graduated;
            return this;
        }

        public Builder grade(int grade) {
            this.nestedGrade = grade;
            return this;
        }

        public EnrollmentsDTO create() {
            return new EnrollmentsDTO(nestedEnrollmentId, nestedStudentId, nestedCourseId,
                    nestedGraduated, nestedGrade);
        }
    }
}
