package backend.dto;

/**
 * Created by mihai on 09.12.2015.
 */
public class UsersDTO {
    private int id;
    private String username;
    private String hashedPassword;
    private String emailAddress;
    private String firstName;
    private String lastName;
    private String role;
    private String type;

    public UsersDTO() {

    }

    public UsersDTO(int id, String username, String hashedPassword,
                 String emailAddress, String firstName, String lastName, String role, String type) {
        this.id = id;
        this.username = username;
        this.hashedPassword = hashedPassword;
        this.emailAddress = emailAddress;
        this.firstName = firstName;
        this.lastName = lastName;
        this.role = role;
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getHashedPassword() {
        return hashedPassword;
    }

    public void setHashedPassword(String hashedPassword) {
        this.hashedPassword = hashedPassword;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public static class Builder {
        private int nestedId;
        private String nestedUsername;
        private String nestedHashedPassword;
        private String nestedEmailAddress;
        private String nestedFirstName;
        private String nestedLastName;
        private String nestedRole;
        private String nestedType;

        public Builder id(int id) {
            this.nestedId = id;
            return this;
        }

        public Builder userName(String userName) {
            this.nestedUsername = userName;
            return this;
        }

        public Builder hashedPasword(String hashedPasword) {
            this.nestedHashedPassword = hashedPasword;
            return this;
        }

        public Builder emailAddress(String emailAddress) {
            this.nestedEmailAddress = emailAddress;
            return this;
        }

        public Builder firstName(String firstName) {
            this.nestedFirstName = firstName;
            return this;
        }

        public Builder lastName(String lastName) {
            this.nestedLastName = lastName;
            return this;
        }

        public Builder role(String role) {
            this.nestedRole = role;
            return this;
        }

        public Builder type(String type) {
            this.nestedType = type;
            return this;
        }

        public UsersDTO create() {
            return new UsersDTO(nestedId, nestedUsername, nestedHashedPassword, nestedEmailAddress,
                    nestedFirstName, nestedLastName, nestedRole, nestedType);
        }
    }
}
