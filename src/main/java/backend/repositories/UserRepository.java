package backend.repositories;

import backend.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

import backend.entities.Userr;

public interface UserRepository extends JpaRepository<Userr, Integer> {

	User findById(int id);
}
