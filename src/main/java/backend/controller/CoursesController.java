package backend.controller;

import backend.dto.UsersDTO;
import backend.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by mihai on 27.12.2015.
 */
@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/courses")
public class CoursesController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/details/{id}", method = RequestMethod.GET)
    public UsersDTO getUsersById(@PathVariable("id") int id) {
        return userService.findUsersById(id);
    }
}
